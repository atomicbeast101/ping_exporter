# Ping Exporter
Prometheus exporter to retrieve ping status using Windows/Unix's built-in ping command.

## Requirements
* Python v3
* Python packages: `prometheus_client pyyaml`

## Guide
1. Install latest Python v3
2. Install Python v3 required packages: `python3 -m pip install prometheus_client pyyaml`
3. Configure `config.yml` to include domains/IPs you want to ping.
4. Run program: `python3 ping_exporter.py --config=/path/to/config.yml`

## Optional Arguments (default value are shown)
`--web.listen-address=0.0.0.0:9100`: Bind IP and port.
`--interval=10`: Specify how long between each ping tests in seconds.

## Example of Metrics
```
# HELP ping_time Ping latency in milliseconds/ms.
# TYPE ping_time gauge
ping_time{host="1.1.1.1"} 17.1
ping_time{host="10.0.1.3"} 0.452
ping_time{host="1.0.0.1"} 17.3
# HELP ping_status Ping success (0=Fail,1=Success)
# TYPE ping_status gauge
ping_status{host="1.1.1.1"} 1.0
ping_status{host="10.0.1.3"} 1.0
ping_status{host="1.0.0.1"} 1.0
```
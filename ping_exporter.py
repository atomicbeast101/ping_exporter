#!/usr/bin/env python3

from prometheus_client import start_http_server, Gauge
from threading import Thread
import subprocess
import platform
import argparse
import time
import yaml
import traceback


# Arguments
parser = argparse.ArgumentParser(description='Prometheus exporter for network speedtests.')
parser.add_argument('--web.listen-address', action='store', dest='listen_addr', help='Specify host and port to display metrics for scraping.')
parser.add_argument('--config', action='store', dest='config_file', help='Full file path to config.yml file.')
parser.add_argument('--interval', action='store', dest='interval', help='Set interval in seconds on how often data should be updated.')


# Attributes
WIN_CMD = 'ping {host} -n 1'
UNIX_CMD = 'ping {host} -c 1'
metrics = {
    'ping_status': Gauge('ping_status', 'Ping success (0=Fail,1=Success)', ['host']),
    'ping_ttl': Gauge('ping_ttl', 'Ping TTL.', ['host']),
    'ping_time': Gauge('ping_time', 'Ping latency in milliseconds/ms.', ['host'])
}
hosts = []


def load_config(_file):
    global hosts

    with open(_file) as config_file:
        config = yaml.safe_load(config_file)
        hosts = config['hosts']


def ping_server(_host):
    global metrics

    cmd = ''
    if platform.system() == 'Windows':
        cmd = WIN_CMD
    elif platform.system() == 'Linux':
        cmd = UNIX_CMD
    try:
        r = subprocess.run(cmd.format(host=_host).split(' '), stdout=subprocess.PIPE)
        data = r.stdout.decode().split(' ')
        metrics['ping_status'].labels(host=_host).set(1)
        for d in data:
            if 'time=' in d:
                if platform.system() == 'Windows':
                    metrics['ping_time'].labels(host=_host).set(float(d[5:-2]))
                elif platform.system() == 'Linux':
                    metrics['ping_time'].labels(host=_host).set(float(d[5:]))
            elif 'TTL=' in d:
                if platform.system() == 'Windows':
                    metrics['ping_ttl'].labels(host=_host).set(float(d[5:-2]))
                elif platform.system() == 'Linux':
                    metrics['ping_ttl'].labels(host=_host).set(float(d[5:]))
    except:
        print(data)
        print(traceback.print_exc())
        metrics['ping_status'].labels(host=_host).set(0)
        metrics['ping_ttl'].labels(host=_host).set(-1)
        metrics['ping_time'].labels(host=_host).set(-1)


def metrics_updater(_interval):
    while True:
        for host in hosts:
            ping_server(host)
        time.sleep(_interval)

# Main
if __name__ == '__main__':
    # Load configuration
    options = parser.parse_args()
    if not options.config_file:
        print('ERROR: You must specify path to config file using --config!')
        exit()
    load_config(options.config_file)

    # Manage arguments
    interval = 10
    if options.interval:
        interval = int(options.interval)
        
    # Start metrics updater
    Thread(target=metrics_updater, args=(interval, )).start()

    # Start HTTP server
    try:
        if options.listen_addr:
            if len(options.listen_addr.split(':')) == 2:
                ip = options.listen_addr.split(':')[0]
                port = int(options.listen_addr.split(':')[-1])
                print('INFO: Listening on {}:{}...'.format(ip, port))
                start_http_server(port, addr=ip)
            else:
                print('ERROR: Invalid web.listen_address value! Must have IP and port separated by : !')
                exit()
        else:
            print('INFO: Listening on {}:{}...'.format('0.0.0.0', 9100))
            start_http_server(9100, addr='0.0.0.0')
    except:
        print('ERROR: Invalid IP for web.listen_address flag!')
        exit()

